# DO NOT USE THIS FILE. YOU MAY COPY ITS CONTENT INTO ANOTHER SCRIPT.

"""
    There are a few libs imported here, their use is only optional
    you may import other libs as well but you need to provide
    a requirements.txt if you use any third-party libs.
"""

import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string  # https://docs.python.org/3.6/library/string.html
import re
import requests


def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

    Complexity levels:
        Complexity == 1: return a password with only lowercase chars
        Complexity ==  2: Previous level plus at least 1 digit
        Complexity ==  3: Previous levels plus at least 1 uppercase char
        Complexity ==  4: Previous levels plus at least 1 punctuation char

    :param length: number of characters
    :param complexity: complexity level
    :returns: generated password
    """
    lowercase = "abcdefghijklmnopqrstuvwxyz"
    number = "0123456789"
    uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    punctuation = "!@#$%^&*"
    passwordArray = []
    
    if complexity == 1:
    
        for i in range(length):
            random_number = random.randrange(len(lowercase))
            passwordArray.append(lowercase[random_number])
        
        passwordString = "".join(passwordArray)
        
    if complexity == 2:
        passwordArray.append(number[random.randrange(len(number))])
        
        concatstr = lowercase+number
    
        for i in range(length-1):
            random_number = random.randrange(len(concatstr))
            passwordArray.append(concatstr[random_number])
        
        random.shuffle(passwordArray)
    
        passwordString = "".join(passwordArray)

    if complexity == 3:
        passwordArray.append(number[random.randrange(len(number))])
        passwordArray.append(uppercase[random.randrange(len(uppercase))])
        
        concatstr = lowercase+number
        concatstr = concatstr+uppercase
    
        for i in range(length-2):
            random_number = random.randrange(len(concatstr))
            passwordArray.append(concatstr[random_number])
        
        random.shuffle(passwordArray)
    
        passwordString = "".join(passwordArray)
        
    if complexity == 4:
        passwordArray.append(number[random.randrange(len(number))])
        passwordArray.append(uppercase[random.randrange(len(uppercase))])
        passwordArray.append(punctuation[random.randrange(len(punctuation))])
        
        concatstr = lowercase+number
        concatstr = concatstr+uppercase
        concatstr = concatstr+punctuation
    
        for i in range(length-3):
            random_number = random.randrange(len(concatstr))
            passwordArray.append(concatstr[random_number])
        
        random.shuffle(passwordArray)
    
        passwordString = "".join(passwordArray)
        
    
    return passwordString
    
    pass

def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

    Complexity levels:
        Return complexity 1: If password has only lowercase chars
        Return complexity 2: Previous level condition and at least 1 digit
        Return complexity 3: Previous levels condition and at least 1 uppercase char
        Return complexity 4: Previous levels condition and at least 1 punctuation

    Complexity level exceptions (override previous results):
        Return complexity 2: password has length >= 8 chars and only lowercase chars
        Return complexity 3: password has length >= 8 chars and only lowercase and digits

    :param password: password
    :returns: complexity level
    """
    
    
    if password.isalpha() and password.islower():
        if len(password) >= 8:
            return 2
        else:
            return 1
   
    if not re.search('[0-9]',password) is None and not re.search('[A-Z]',password) is None and re.search('[!@#$%^&*]',password) is None:
            return 3
    
    if not re.search('[0-9]',password) is None and not re.search('[A-Z]',password) is None and not re.search('[!@#$%^&*]',password) is None:
            return 4
       
    if not re.search('[0-9a-z]',password) is None and re.search('[A-Z]',password) is None:
        if len(password) >= 8:
            return 3
        else:
            return 2
    
    pass


def get_random_user() :
    
    randomuser = requests.get('https://randomuser.me/api/')
    random_json = randomuser.json()
    
    first_name = random_json['results'][0]['name']['first']
    last_name = random_json['results'][0]['name']['last']
    
    email = random_json['results'][0]['email']
    
    name = first_name+" "+last_name
    
    stringarray = []
    
    stringarray.append(name)
    stringarray.append(email)
    
    return stringarray
    
    
    pass

count = 1

def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
    :return: None
    """
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    
    global count
    
    c.execute('CREATE TABLE IF NOT EXISTS userlogin (id Int,name Varchar, email Varchar,password Varchar)')
    
    userdetail = get_random_user()    
        
    c.execute("insert into userlogin(id,name,email,password) values(?,?,?,?)",(count,userdetail[0],userdetail[1],"none"))
    
    count = count+1
    
    conn.commit()
    conn.close()    
    
    return None
    
    pass


def create_userlogin(db_path)->None:
 
   for x in range(10):
       create_user(db_path)
         
   conn = sqlite3.connect(db_path)
   c = conn.cursor()    
    
   for x in range(10):
       c.execute("update userlogin SET password = ? where id = ?", (generate_password(random.randrange(6,12),random.randrange(1,4)),x))
       conn.commit()
    
    
   conn.close()
    
   return None
    