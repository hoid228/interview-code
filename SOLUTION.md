generate_password()
The algorithm for this function was pretty straight forward. I used random package to meet the various specifications of complexity of including lowercase alphabets, numbers, uppercase alphabets and punctuations, all of these been taken as separate string.

check_password()
I had to import 're' package inorder to make use of regex which would be an easier and efficient way to determine their complexity

create_user()
This module makes use of a function called get_user(). I have created this function solely for the purpose of JSON parsing and to provide reusability to the set of codes. The 'requests' package has to be imported for the purpose of JSON URL request parsing.

create_userlogin()
this method creates 10 users using the above methods and random passwords are generated and assigned to these users. The results are stored in a table called 'userlogin' in the required DB scheme given as input.